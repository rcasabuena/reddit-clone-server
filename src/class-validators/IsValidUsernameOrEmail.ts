import { ValidationOptions, registerDecorator } from "class-validator";
import { User } from "../entities/User";

export function IsValidUsernameOrEmail(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: "IsValidUsernameOrEmail",
      target: object.constructor,
      propertyName: propertyName,
      constraints: [],
      options: validationOptions,
      validator: {
        async validate(value: any) {
          const user = User.findOne({
            where: [{ username: value }, { email: value }],
          });

          if (!user) return false;

          return true;
        },
      },
    });
  };
}
