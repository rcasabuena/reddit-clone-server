import {
  ValidationOptions,
  registerDecorator,
  ValidationArguments,
} from "class-validator";
import { User } from "../entities/User";

export function IsUniqueUsername(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: "IsUniqueUsername",
      target: object.constructor,
      propertyName: propertyName,
      constraints: [],
      options: validationOptions,
      validator: {
        async validate(value: any, args: ValidationArguments) {
          const user = await User.findOne({ where: { username: value } });

          if (!user) return true;

          return false;
        },
      },
    });
  };
}
