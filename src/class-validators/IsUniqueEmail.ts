import {
  ValidationOptions,
  registerDecorator,
  ValidationArguments,
} from "class-validator";
import { User } from "../entities/User";

export function IsUniqueEmail(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: "IsUniqueEmail",
      target: object.constructor,
      propertyName: propertyName,
      constraints: [],
      options: validationOptions,
      validator: {
        async validate(value: any, args: ValidationArguments) {
          const user = await User.findOne({ where: { email: value } });

          if (!user) return true;

          return false;
        },
      },
    });
  };
}
