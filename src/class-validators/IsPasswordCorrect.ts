import {
  ValidationOptions,
  registerDecorator,
  ValidationArguments,
} from "class-validator";
import { User } from "../entities/User";
import { UserLoginInput } from "../input-types/user/UserLoginInput";
import argon2 from "argon2";

export function IsPasswordCorrect(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: "IsPasswordCorrect",
      target: object.constructor,
      propertyName: propertyName,
      constraints: [],
      options: validationOptions,
      validator: {
        async validate(value: any, args: ValidationArguments) {
          const username = (args.object as UserLoginInput).username;
          if (username) {
            const user = await User.findOne({
              where: [{ username }, { email: username }],
            });

            if (user) {
              const valid = await argon2.verify(user.password, value);
              if (!valid) return false;
            }
          }

          return true;
        },
      },
    });
  };
}
