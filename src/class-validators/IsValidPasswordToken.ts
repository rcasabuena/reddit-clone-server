import { redis } from "../redis";
import {
  ValidationOptions,
  registerDecorator,
  ValidationArguments,
} from "class-validator";
import { FORGOT_PASSWORD_PREFIX } from "../constants";
import { User } from "../entities/User";

export function IsValidPasswordToken(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: "IsValidPasswordToken",
      target: object.constructor,
      propertyName: propertyName,
      constraints: [],
      options: validationOptions,
      validator: {
        async validate(value: any, args: ValidationArguments) {
          const id = await redis.get(`${FORGOT_PASSWORD_PREFIX}:${value}`);

          if (!id) return false;

          const user = await User.findOne(id);

          if (!user) return false;

          return true;
        },
      },
    });
  };
}
