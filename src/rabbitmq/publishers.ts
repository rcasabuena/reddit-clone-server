import amqplib from "amqplib";
import { RABBITMQ_HOST } from "../constants";

export async function RabbitMQSendMessageToQueue(queue: string, msg: any) {
  if (RABBITMQ_HOST) {
    try {
      const conn = await amqplib.connect(RABBITMQ_HOST);
      const ch = await conn.createChannel();
      await ch.assertQueue(queue);
      await ch.sendToQueue(queue, Buffer.from(JSON.stringify(msg)));
      console.log(`message sent successfully ${queue}`);
      await ch.close();
      await conn.close();
    } catch (err) {
      console.error(err);
      throw new Error(err);
    }
  }
}
