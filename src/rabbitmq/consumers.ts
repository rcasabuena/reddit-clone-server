import amqplib from "amqplib";
import { sendEmailWithTemplates } from "../utils/sendEmailWithTemplates";
import { EMAIL_QUEUE, RABBITMQ_HOST } from "../constants";

export async function RabbitMQEmailQueue() {
  if (RABBITMQ_HOST) {
    try {
      const conn = await amqplib.connect(RABBITMQ_HOST);
      const ch = await conn.createChannel();
      await ch.assertQueue(EMAIL_QUEUE);

      ch.consume(EMAIL_QUEUE, (msg) => {
        if (msg) {
          const data = JSON.parse(msg.content.toString());
          try {
            sendEmailWithTemplates(data);
            ch.ack(msg);
          } catch (err) {
            console.error(err);
            throw new Error(err);
          }
        }
      });
    } catch (err) {
      console.error(err);
      throw new Error(err);
    }
  }
}
