import { Post } from "../entities/Post";
import {
  Arg,
  Ctx,
  ID,
  Mutation,
  Query,
  Resolver,
  UseMiddleware,
} from "type-graphql";
import { PostInput } from "../input-types/post/PostInput";
import { MyContext } from "../types";
import { User } from "../entities/User";
import { isAuth } from "../middleware/isAuth";
import { PostResponse } from "../object-types/PostResponse";
import { validateFields } from "../utils/validateFields";

@Resolver()
export class PostResolver {
  @Query(() => [Post])
  posts(): Promise<Post[]> {
    return Post.find();
  }

  @Query(() => Post, { nullable: true })
  post(@Arg("id", () => ID) id: string): Promise<Post | undefined> {
    return Post.findOne(id);
  }

  @Mutation(() => PostResponse)
  @UseMiddleware(isAuth)
  async createPost(
    @Arg("options") options: PostInput,
    @Ctx() { req }: MyContext
  ): Promise<PostResponse> {
    const errors = await validateFields(options);
    if (errors) {
      return {
        errors,
      };
    }

    return {
      post: await Post.create({
        ...options,
        creator: await User.findOne(req.session.userId),
      }).save(),
    };
  }

  @Mutation(() => Post, { nullable: true })
  async updatePost(
    @Arg("id", () => ID) id: string,
    @Arg("title", () => String, { nullable: true }) title: string
  ): Promise<Post | undefined> {
    const post = await Post.findOne(id);
    if (!post) return undefined;

    if (typeof title !== undefined) {
      await Post.update({ id }, { title });
    }

    return post;
  }

  @Mutation(() => Boolean)
  async deletePost(@Arg("id", () => ID) id: string): Promise<boolean> {
    await Post.delete(id);
    return true;
  }
}
