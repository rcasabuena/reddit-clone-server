import { UserRegisterInput } from "../input-types/user/UserRegisterInput";
import { Arg, Ctx, Mutation, Query, Resolver } from "type-graphql";
import argon2 from "argon2";
import { MyContext } from "../types";
import { User } from "../entities/User";
import { UserLoginInput } from "../input-types/user/UserLoginInput";
import { UserResponse } from "../object-types/UserResponse";
import { COOKIE_NAME, FORGOT_PASSWORD_PREFIX } from "../constants";
import { emitter } from "../emitter";
import { ForgotPasswordInput } from "../input-types/user/ForgotPasswordInput";
import { ForgotPasswordResponse } from "../object-types/ForgotPasswordResponse";
import { validateFields } from "../utils/validateFields";
import { ChangePasswordInput } from "../input-types/user/ChangePasswordInput";

@Resolver()
export class UserResolver {
  @Mutation(() => ForgotPasswordResponse)
  async forgotPassword(
    @Arg("options") options: ForgotPasswordInput,
    @Ctx() { req }: MyContext
  ) {
    const errors = await validateFields(options);
    if (errors) {
      return {
        errors,
        ok: false,
      };
    }

    const { email } = options;
    const user = await User.findOne({ where: { email } });

    if (user) {
      emitter.emit("forgotPasswordRequest", {
        user,
        origin: req.get("origin"),
      });
    }

    return {
      ok: true,
    };
  }

  @Mutation(() => UserResponse)
  async changePassword(
    @Arg("options") options: ChangePasswordInput,
    @Ctx() { redis, req }: MyContext
  ): Promise<UserResponse> {
    const errors = await validateFields(options);
    if (errors) {
      return {
        errors,
      };
    }

    const { token, newPassword } = options;
    const key = `${FORGOT_PASSWORD_PREFIX}:${token}`;
    const id = await redis.get(key);
    const user = await User.findOne(id!);

    const password = await argon2.hash(newPassword);
    await User.update({ id: id! }, { password });
    await redis.del(key);

    // Log user in after password change
    req.session.userId = user!.id;

    return {
      user: user!,
    };
  }

  @Query(() => User, { nullable: true })
  currentUser(@Ctx() { req }: MyContext) {
    // No current loggedin user
    if (!req.session.userId) return null;

    return User.findOne(req.session.userId);
  }

  @Mutation(() => UserResponse)
  async register(
    @Arg("options") options: UserRegisterInput,
    @Ctx() { req }: MyContext
  ): Promise<UserResponse> {
    const errors = await validateFields(options);
    if (errors) {
      return {
        errors,
      };
    }

    const { username, email, password } = options;

    const hashedPassword = await argon2.hash(password);
    const user = await User.create({
      username,
      email,
      password: hashedPassword,
    }).save();

    // Log user in after registration
    req.session.userId = user.id;

    return {
      user,
    };
  }

  @Mutation(() => UserResponse)
  async login(
    @Arg("options") options: UserLoginInput,
    @Ctx() { req }: MyContext
  ): Promise<UserResponse> {
    const errors = await validateFields(options);
    if (errors) {
      return {
        errors,
      };
    }

    const { username } = options;

    const user = await User.findOne({
      where: [{ username }, { email: username }],
    });

    req.session.userId = user!.id;

    //emitter.emit("userLoggedIn", { user });

    return {
      user: user!,
    };
  }

  @Mutation(() => Boolean)
  logout(@Ctx() { req, res }: MyContext) {
    return new Promise((resolve) => {
      req.session.destroy((err) => {
        res.clearCookie(COOKIE_NAME);
        if (err) {
          console.log(err);
          resolve(false);
          return;
        }
        resolve(true);
      });
    });
  }
}
