import Email from "email-templates";
import { APP_EMAIL_SENDER } from "./constants";

export const emailTemplates = (transporter: any) => {
  return new Email({
    message: {
      from: APP_EMAIL_SENDER,
    },
    send: true,
    preview: false,
    transport: transporter,
    views: {
      options: {
        extension: "ejs",
      },
    },
  });
};
