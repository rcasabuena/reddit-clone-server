import Redis from "ioredis";
import { REDIS_PORT, REDIS_HOST, REDIS_PASSWORD } from "./constants";

export const redis = new Redis({
  port: REDIS_PORT,
  host: REDIS_HOST,
  family: 4, // 4 (IPv4) or 6 (IPv6)
  password: REDIS_PASSWORD,
  db: 0,
} as any);
