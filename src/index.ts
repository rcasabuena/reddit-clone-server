import "reflect-metadata";
import express from "express";
import { SERVER_PORT, X_POWERED_BY } from "./constants";
import { ApolloServer } from "apollo-server-express";
import { buildSchema } from "type-graphql";
import { HelloResolver } from "./resolvers/hello";
import { PostResolver } from "./resolvers/post";
import { UserResolver } from "./resolvers/user";
import { session } from "./session";
import { redis } from "./redis";
import { MyContext } from "./types";
import cors from "cors";
import { RabbitMQEmailQueue } from "./rabbitmq/consumers";
import { createConnection } from "typeorm";
import TypeORMConfig from "./typeorm.config";

const main = async () => {
  await createConnection(TypeORMConfig);

  const app = express();
  app.use((_, res, next) => {
    res.setHeader("X-Powered-By", X_POWERED_BY);
    next();
  });
  app.use(
    cors({
      origin: "http://localhost:3000",
      credentials: true,
    })
  );
  app.use(session);

  const apolloServer = new ApolloServer({
    schema: await buildSchema({
      resolvers: [HelloResolver, PostResolver, UserResolver],
      validate: false,
    }),
    context: ({ req, res }): MyContext => ({ req, res, redis }),
  });

  apolloServer.applyMiddleware({
    app,
    cors: false,
  });

  // RabbitMQ Consumers
  RabbitMQEmailQueue();

  app.listen(SERVER_PORT, () => {
    console.log(`server started on localhost port ${SERVER_PORT}`);
  });
};

main();
