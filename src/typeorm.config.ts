import { ConnectionOptions } from "typeorm";
import {
  IS_PROD,
  POSTGRESQL_DATABASE,
  POSTGRESQL_HOST,
  POSTGRESQL_PORT,
  POSTGRESQL_USERNAME,
  POSTGRESQL_PASSWORD,
} from "./constants";
import { Post } from "./entities/Post";
import { User } from "./entities/User";

export default {
  name: "default",
  type: "postgres",
  port: POSTGRESQL_PORT,
  host: POSTGRESQL_HOST,
  username: POSTGRESQL_USERNAME,
  password: POSTGRESQL_PASSWORD,
  database: POSTGRESQL_DATABASE,
  entities: [Post, User],
  synchronize: true,
  logging: !IS_PROD,
} as ConnectionOptions;
