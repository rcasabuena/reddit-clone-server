import Session from "express-session";
import connectRedis from "connect-redis";
import { redis } from "./redis";
import { COOKIE_NAME, IS_PROD, SESSION_SECRET } from "./constants";

declare module "express-session" {
  export interface SessionData {
    userId: string;
  }
}

const RedisStore = connectRedis(Session);

export const session = Session({
  name: COOKIE_NAME,
  store: new RedisStore({ client: redis, disableTouch: true }),
  secret: SESSION_SECRET,
  resave: false,
  saveUninitialized: false,
  cookie: {
    maxAge: 1000 * 60 * 60 * 5, // 5 hours
    httpOnly: true,
    sameSite: "lax", // csrf
    secure: IS_PROD,
  },
});
