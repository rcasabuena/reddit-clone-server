export const NODE_ENV = process.env.NODE_ENV;
export const SERVER_PORT = parseInt(process.env.PORT!) || 3000;
export const SESSION_SECRET = process.env.SESSION_SECRET || "badtz maru";
export const X_POWERED_BY = process.env.X_POWERED_BY || "";
export const COOKIE_NAME = "qid";

export const POSTGRESQL_HOST = process.env.POSTGRESQL_HOST;
export const POSTGRESQL_PORT = parseInt(process.env.POSTGRESQL_PORT!);
export const POSTGRESQL_DATABASE = process.env.POSTGRESQL_DATABASE;
export const POSTGRESQL_USERNAME = process.env.POSTGRESQL_USERNAME;
export const POSTGRESQL_PASSWORD = process.env.POSTGRESQL_PASSWORD;

export const REDIS_HOST = process.env.REDIS_HOST;
export const REDIS_PORT = parseInt(process.env.REDIS_PORT!);
export const REDIS_PASSWORD = process.env.REDIS_PASSWORD;

export const FORGOT_PASSWORD_PREFIX = "forgot-password-token";

export const SENDGRID_API_KEY = process.env.SENDGRID_API_KEY;

export const RABBITMQ_HOST = process.env.RABBITMQ_HOST;
export const EMAIL_QUEUE = "emails";

export const APP_EMAIL_SENDER = process.env.APP_EMAIL_SENDER;

export const IS_PROD = process.env.NODE_ENV === "production";
