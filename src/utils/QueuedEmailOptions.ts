export interface QueuedEmailOptions {
  from?: string;
  to: string;
  subject: string;
  template: string;
  context?: any;
}
