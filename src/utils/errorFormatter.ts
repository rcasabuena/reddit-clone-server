import { ValidationError } from "class-validator";

export const errorFormatter = (errors: ValidationError[]): any => {
  return errors.map((item) => {
    const messages: Array<string> = Object.values(item.constraints as Object);
    return {
      field: item.property,
      message: messages[0],
    };
  });
};
