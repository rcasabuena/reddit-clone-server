import { validate } from "class-validator";
import { FieldError } from "../object-types/FieldError";
import { errorFormatter } from "./errorFormatter";

export async function validateFields(
  options: object
): Promise<FieldError[] | null> {
  const err = await validate(options);
  if (err.length > 0) {
    return errorFormatter(err);
  }
  return null;
}
