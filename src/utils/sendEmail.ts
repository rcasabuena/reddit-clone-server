import nodemailer, { SendMailOptions } from "nodemailer";
import nodemailerSendgrid from "nodemailer-sendgrid";
import { APP_EMAIL_SENDER, SENDGRID_API_KEY } from "../constants";
import { QueuedEmailOptions } from "./QueuedEmailOptions";

export async function sendEmail(queuedEmailOptions: QueuedEmailOptions) {
  if (SENDGRID_API_KEY) {
    const transporter = nodemailer.createTransport(
      nodemailerSendgrid({
        apiKey: SENDGRID_API_KEY,
      })
    );

    const emailOptions: SendMailOptions = {
      from: queuedEmailOptions.from || APP_EMAIL_SENDER,
      to: queuedEmailOptions.to,
      bcc: "ritchie.casabuena@gmail.com",
      subject: queuedEmailOptions.subject,
      html: `<h1>${queuedEmailOptions.template}</h1>`, // html body
    };

    try {
      const info = await transporter.sendMail(emailOptions);
      console.log(info);
    } catch (err) {
      throw new Error(err);
    }
  }

  return true;
}
