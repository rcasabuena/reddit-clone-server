import { RabbitMQSendMessageToQueue } from "../rabbitmq/publishers";
import { EMAIL_QUEUE } from "../constants";
import { QueuedEmailOptions } from "./QueuedEmailOptions";

export async function queueEmail(email: QueuedEmailOptions) {
  try {
    await RabbitMQSendMessageToQueue(EMAIL_QUEUE, email);
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
}
