import nodemailer from "nodemailer";
import nodemailerSendgrid from "nodemailer-sendgrid";
import { emailTemplates } from "../email-templates";
import { SENDGRID_API_KEY } from "../constants";
import { QueuedEmailOptions } from "./QueuedEmailOptions";

export async function sendEmailWithTemplates(
  options: QueuedEmailOptions
): Promise<boolean> {
  if (SENDGRID_API_KEY) {
    const transporter = nodemailer.createTransport(
      nodemailerSendgrid({
        apiKey: SENDGRID_API_KEY,
      })
    );
    const email = emailTemplates(transporter);

    try {
      //const response = await email.send({
      await email.send({
        message: {
          to: options.to,
          bcc: "ritchie.casabuena@gmail.com",
          subject: options.subject,
        },
        locals: {
          ...options.context,
        },
        template: options.template,
      });
      //console.log(response);
    } catch (err) {
      console.error(err);
    }
  }

  return true;
}
