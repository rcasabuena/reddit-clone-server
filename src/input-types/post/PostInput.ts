import { Field, InputType } from "type-graphql";
import { MinLength } from "class-validator";

@InputType()
export class PostInput {
  @Field()
  @MinLength(5)
  title!: string;

  @Field()
  @MinLength(5)
  text!: string;
}
