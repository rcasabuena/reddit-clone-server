import { IsValidUsernameOrEmail } from "../../class-validators/IsValidUsernameOrEmail";
import { Field, InputType } from "type-graphql";
import { IsPasswordCorrect } from "../../class-validators/IsPasswordCorrect";

@InputType()
export class UserLoginInput {
  @Field()
  @IsValidUsernameOrEmail({ message: "invalid username or email" })
  username!: string;

  @Field()
  @IsPasswordCorrect({ message: "incorrect password" })
  password!: string;
}
