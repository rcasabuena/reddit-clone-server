import { Field, InputType } from "type-graphql";
import { MinLength } from "class-validator";
import { IsValidPasswordToken } from "../../class-validators/IsValidPasswordToken";

@InputType()
export class ChangePasswordInput {
  @Field()
  @MinLength(5, {
    message: "password must be longer than or equal to 5 characters",
  })
  newPassword!: string;

  @Field()
  @IsValidPasswordToken({ message: "invalid token" })
  token!: string;
}
