import { Field, InputType } from "type-graphql";
import { IsEmail, MinLength } from "class-validator";
import { IsUniqueUsername } from "../../class-validators/IsUniqueUsername";
import { IsUniqueEmail } from "../../class-validators/IsUniqueEmail";

@InputType()
export class UserRegisterInput {
  @Field()
  @MinLength(5)
  @IsUniqueUsername({ message: "username is already in use" })
  username!: string;

  @Field()
  @IsEmail()
  @IsUniqueEmail({ message: "email is already in use" })
  email!: string;

  @Field()
  @MinLength(5)
  password!: string;
}
