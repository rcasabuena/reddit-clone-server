import EventEmitter from "events";
import { queueEmail } from "./utils/queueEmail";
import { v4 } from "uuid";
import { redis } from "./redis";
import { FORGOT_PASSWORD_PREFIX } from "./constants";

export const emitter = new EventEmitter();

emitter.on("userLoggedIn", ({ user }) => {
  try {
    queueEmail({
      to: `${user.email}`, // list of receivers
      subject: "User logged in", // Subject line
      template: "index",
      context: {
        user: {
          ...user,
          password: undefined,
        },
      },
    });
  } catch (err) {
    console.log(err);
  }
});

emitter.on("forgotPasswordRequest", async ({ user, origin }) => {
  const token = v4();
  const url = `${origin}/change-password/${token}`;
  await redis.set(
    `${FORGOT_PASSWORD_PREFIX}:${token}`,
    user.id,
    "ex",
    60 * 60 * 24
  );

  try {
    queueEmail({
      to: `${user.email}`, // list of receivers
      subject: "Password Request", // Subject line
      template: "password-request",
      context: {
        user: {
          ...user,
          password: undefined,
        },
        url,
      },
    });
  } catch (err) {
    console.error(err);
  }
});
